<div class="meta">
	<div class="cat">
		<?php
			$categories = get_the_category();
			$cat = $categories[0];
		?>

		<h5><a href="<?php echo get_term_link( $cat ); ?>"><?php echo $cat->name; ?></a></h5>
	</div>

	<div class="date">
		<h5><span class="new">New!</span> <?php the_time('F d, Y'); ?></h5>
	</div>						
</div>
<div class="photo-wrapper">
	<a href="<?php the_permalink(); ?>">
		<span class="content">
			<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" />
		</span>		
	</a>
</div>
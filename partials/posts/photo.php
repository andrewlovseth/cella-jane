<div class="photo-wrapper">
	<a href="<?php the_permalink(); ?>">
		<span class="content">
			<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" alt="" />
		</span>		
	</a>
</div>
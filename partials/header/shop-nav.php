<div class="shop-nav subnav">
	<div class="subnav-wrapper">
		
		<?php if(have_rows('shop_nav_links', 'options')): while(have_rows('shop_nav_links', 'options')): the_row(); ?>

			<div class="subnav-link">
			    <a href="<?php the_sub_field('link'); ?>">
			    	<?php the_sub_field('label'); ?>        
			    </a>			
			</div>

		<?php endwhile; endif; ?>			
	</div>
</div>
<?php $main_cat = get_sub_field('main_category'); ?>

<div class="dropdown" id="subnav-<?php echo $main_cat->slug; ?>">
	<div class="dropdown-wrapper">

		<div class="browse">
			<div class="headline nav-headline">
				<h3><a href="<?php echo get_term_link( $main_cat ); ?>"><?php echo $main_cat->name; ?></a></h3>
			</div>

			<div class="sub-categories">
				<?php $terms = get_sub_field('sub_categories'); if( $terms ): ?>
					<?php foreach( $terms as $term ): ?>
						<a href="<?php echo get_term_link( $term ); ?>"><?php echo $term->name; ?></a>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

			<div class="cta">
				<a href="<?php echo get_term_link( $main_cat ); ?>" class="btn">View All</a>
			</div>								
		</div>

		<div class="posts">
			<?php $posts = get_sub_field('curated_posts'); if( $posts ): ?>
				<?php foreach( $posts as $p ): ?>

					<article class="post nav-post">
						<div class="photo">
							<div class="photo-wrapper">
								<a href="<?php echo get_permalink( $p->ID ); ?>">
									<span class="content">
										<img src="<?php echo get_the_post_thumbnail_url($p->ID, 'medium'); ?>" alt="" />
									</span>		
								</a>
							</div>
						</div>

						<div class="info">
							<div class="headline">
								<h3><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h3>
							</div>
						</div>
					</article>

				<?php endforeach; ?>
			<?php endif; ?>
		</div>	

	</div>	
</div> 


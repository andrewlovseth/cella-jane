<nav class="mobile-nav">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="mobile-nav-links">
				<?php if(have_rows('desktop_nav_links', 'options')): while(have_rows('desktop_nav_links',  'options')) : the_row(); ?>

				    <?php if( get_row_layout() == 'section' ): ?>

						<?php $main_cat = get_sub_field('main_category'); ?>			

						<div class="mobile-nav-link">
							<a href="<?php echo get_term_link( $main_cat ); ?>"><?php echo $main_cat->name; ?></a>
						</div>
					
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>		
			
				<?php if(have_rows('utility_nav_links', 'options')): while(have_rows('utility_nav_links', 'options')): the_row(); ?>
					<?php $label = sanitize_title_with_dashes(get_sub_field('label')); ?>

					<div class="mobile-nav-link <?php echo $label; ?>-link">
					    <a href="<?php the_sub_field('link'); ?>" class="<?php echo $label; ?>-trigger"><?php the_sub_field('label'); ?></a>
					</div> 

				<?php endwhile; endif; ?>		

			</div>			

		</div>
	</div>
</nav>
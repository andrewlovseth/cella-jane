<?php if(have_rows('social_nav_links', 'options')): while(have_rows('social_nav_links', 'options')): the_row(); ?>

    <a href="<?php the_sub_field('link'); ?>" rel="external">
    	<span class="icon">
    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    	</span>			        
    </a>

<?php endwhile; endif; ?>

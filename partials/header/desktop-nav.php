<nav class="desktop-nav">
	<div class="desktop-links">

		<?php if(have_rows('desktop_nav_links', 'options')): while(have_rows('desktop_nav_links',  'options')) : the_row(); ?>
		    <?php if( get_row_layout() == 'section' ): $main_cat = get_sub_field('main_category'); ?>			

				<div class="main-link">
					<a href="<?php echo get_term_link( $main_cat ); ?>"><?php echo $main_cat->name; ?></a>

					<?php get_template_part('partials/header/desktop-dropdowns'); ?>
				</div>

		    <?php endif; ?>		 
		<?php endwhile; endif; ?>
		
	</div>
</nav>

<nav class="social-nav-links">
	<?php get_template_part('partials/header/social-nav-icon-links'); ?>

	<?php if(get_field('email', 'options')): ?>
		<a href="mailto:<?php the_field('email', 'options'); ?>">
			<img src="<?php bloginfo('template_directory') ?>/images/email-icon.svg" alt="Search Icon" />
		</a>
	<?php endif; ?>
</nav>
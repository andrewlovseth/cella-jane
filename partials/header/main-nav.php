<header class="site-header">
	<div class="wrapper">

		<div class="site-logo">
			<a href="<?php echo site_url('/'); ?>">
				<img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>

		<?php get_template_part('partials/header/hamburger'); ?>

		<?php get_template_part('partials/header/mobile-nav'); ?>

		<?php get_template_part('partials/header/desktop-nav'); ?>

	</div>
</header>
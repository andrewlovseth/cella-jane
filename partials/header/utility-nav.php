<nav class="utility-nav">
	<div class="wrapper">
		
		<div class="nav-search">
			<a href="#" class="search-trigger">
				<img src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" alt="Search Icon" />
			</a>			
		</div>	

		<div class="utility-nav-links">
			<?php if(have_rows('utility_nav_links', 'options')): while(have_rows('utility_nav_links', 'options')): the_row(); ?>

				<?php $label = sanitize_title_with_dashes(get_sub_field('label')); ?>

				<div class="link link-<?php echo $label; ?>">
				    <a href="<?php the_sub_field('link'); ?>" class="trigger-<?php echo $label; ?>">
				    	<?php if(get_sub_field('icon')): ?>
				    		<span class="icon">
				    			<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    		</span>			    		
				    	<?php endif; ?>

				    	<span class="label">
				    		<?php the_sub_field('label'); ?>
				    	</span>			        
				    </a>

				    <?php if($label == 'shop'): ?>
				    	<?php get_template_part('partials/header/shop-nav'); ?>
				    <?php endif; ?>			
				</div>

			<?php endwhile; endif; ?>			
		</div>

	</div>
</nav>
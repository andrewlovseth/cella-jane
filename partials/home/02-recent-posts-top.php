<?php
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 4,
		'offset' => 1
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : ?>
	
	<section class="recent-posts recent-posts-top">
		<div class="bg"></div>
		<div class="wrapper">
			
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				
				<article class="post vertical">
					<div class="photo">
						<?php get_template_part('partials/posts/photo'); ?>
					</div>

					<div class="info">
						<?php get_template_part('partials/posts/meta'); ?>

						<?php get_template_part('partials/posts/title'); ?>
					</div>
				</article>		

			<?php endwhile; ?>

		</div>
	</section>

<?php endif; wp_reset_postdata(); ?>
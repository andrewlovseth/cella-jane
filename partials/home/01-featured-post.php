<?php
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 1
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : ?>
	
	<section class="featured-post">
		<div class="wrapper">
			
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				
				<article class="post featured">
					<div class="photo">
						<?php get_template_part('partials/posts/featured-post/photo'); ?>
					</div>

					<div class="info">
						<div class="info-wrapper">
							<?php get_template_part('partials/posts/featured-post/meta'); ?>

							<?php get_template_part('partials/posts/featured-post/title'); ?>

							<?php get_template_part('partials/posts/featured-post/cta'); ?>							
						</div>
					</div>
				</article>		

			<?php endwhile; ?>

		</div>
	</section>

<?php endif; wp_reset_postdata(); ?>
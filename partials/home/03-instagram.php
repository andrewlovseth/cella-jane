<section class="instagram-main">
	<div class="wrapper">
		
		<div class="headline section-header">
			<h5><a href="#">Shop Here</a></h5>
			<h2>Recently on Instagram</h2>
		</div>

		<div class="feed">
			<?php echo do_shortcode('[instagram-feed]'); ?>
		</div>


	</div>
</section>
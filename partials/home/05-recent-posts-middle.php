<?php
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 2,
		'offset' => 5
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : ?>
	
	<section class="recent-posts recent-posts-middle">
		
		<?php $count = 6; while ( $query->have_posts() ) : $query->the_post(); ?>
			
			<article class="post post-<?php echo $count; ?>">
				<div class="wrapper">
					<div class="photo">
						<?php get_template_part('partials/posts/photo'); ?>
					</div>

					<div class="info">

						<div class="meta meta-top">
							<div class="comments">
								<h5>
									<a href="#">
										<span class="icon">
											<img src="<?php bloginfo('template_directory') ?>/images/comment-bubble.svg" alt="Comment Bubble" />
										</span>
										<span class="label">Comments</span>
									</a>
								</h5>								
							</div>

							<div class="cat">
								<?php
									$categories = get_the_category();
									$cat = $categories[0];
								?>

								<h5><a href="<?php echo get_term_link( $cat ); ?>"><?php echo $cat->name; ?></a></h5>
							</div>							
						</div>


						<?php get_template_part('partials/posts/title'); ?>

						<?php get_template_part('partials/posts/meta'); ?>

						<div class="excerpt">
							<?php
								// Fetch post content
								$content = get_post_field( 'post_content', get_the_ID() );

								// Get content parts
								$content_parts = get_extended( $content );

								// Output part before <!--more--> tag
								echo $content_parts['main'];
							?>
						</div>

						<div class="cta">
							<a class="btn" href="<?php the_permalink(); ?>">Read More</a>

							<div class="signature">
								<img src="<?php bloginfo('template_directory') ?>/images/sig-dark.svg" alt="XX, Becky" />
							</div>
						</div>

					</div>
				</div>
			</article>		

		<?php $count++; endwhile; ?>

	</section>

<?php endif; wp_reset_postdata(); ?>

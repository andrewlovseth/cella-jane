<section class="current-look">
	<div class="wrapper">
	
		<div class="section-header">
			<div class="month">
				<h5><?php the_field('current_look_month', 'options'); ?></h5>
			</div>

			<div class="headline">
				<img src="<?php $image = get_field('current_look_headline', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="shop-all">
				<a href="<?php the_field('current_look_shop_all_link', 'options'); ?>" rel="external">Shop All</a>
			</div>
		</div>

		<div class="look">
			<div class="look-photo">
				<img src="<?php $image = get_field('current_look_photo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="items">
				<?php if(have_rows('current_look_items', 'options')): while(have_rows('current_look_items', 'options')): the_row(); ?>
				 
				    <div class="item">
				    	<div class="photo">
				    		<a href="<?php the_sub_field('link'); ?>" rel="external">
				    			<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    		</a>
				    	</div>

				    	<div class="info">
				    		<div class="headline">
				    			<h3>
				    				<a href="<?php the_sub_field('link'); ?>" rel="external">
				    					<?php the_sub_field('name'); ?>
				    				</a>
				    			</h3>
				    		</div>

				    		<div class="copy p3">
				    			<?php the_sub_field('description'); ?>
				    		</div>
				    	</div>				        
				    </div>

				<?php endwhile; endif; ?>
			</div>
		</div>

	</div>
</section>
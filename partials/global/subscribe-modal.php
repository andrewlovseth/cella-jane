<section id="subscribe-modal">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="photo">
				<img src="<?php $image = get_field('subscribe_photo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<div class="headline">
					<div class="signature">
						<img src="<?php $image = get_field('subscribe_signature', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<h4><?php the_field('subscribe_sub_headline', 'options'); ?></h4>
					<h3><?php the_field('subscribe_headline', 'options'); ?></h3>
				</div>

				<div class="copy">
					<?php the_field('subscribe_copy', 'options'); ?>
				</div>

				<div class="form-wrapper">
					<?php the_field('subscribe_form', 'options'); ?>
				</div>				
			</div>

			<a href="#" class="subscribe-close"></a>

		</div>
	</div>
</section>
<section id="search-modal">
	<div class="overlay">
		<div class="overlay-wrapper">

			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		        <input type="search" class="search-query" autocomplete="off" placeholder="Search" name="s" />
		        <input type="submit" class="search-submit" value="Enter" />
		    </form>

			<a href="#" class="search-close"></a>

		</div>
	</div>
</section>
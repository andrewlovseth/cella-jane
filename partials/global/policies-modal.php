<section id="policies-modal">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="headline">
					<h4><?php the_field('policies_sub_headline', 'options'); ?></h4>
					<h3><?php the_field('policies_headline', 'options'); ?></h3>
				</div>

				<div class="copy">
					<?php the_field('policies_copy', 'options'); ?>
				</div>			
			</div>

			<a href="#" class="policies-close"></a>

		</div>
	</div>
</section>
<section class="credits">
	<div class="signature">
		<img src="<?php $image = get_field('footer_signature', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<div class="back-to-top">
		<a href="#top" class="smooth">Back to top</a>
	</div>

	<div class="copyright copy p4">
		<p><?php the_field('footer_copyright', 'options'); ?></p>	
	</div>

	<div class="site-by copy p4">
		<?php the_field('footer_credits', 'options'); ?>
	</div>	
</section>
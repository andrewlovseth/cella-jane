<!-- INSTAGRAM FEED GOES HERE -->

<section class="footer-instagram">

	<div class="top-liketoknowit">
		<div class="top-liketoknowit-inner">
			<!-- Begin LTK Widget -->
			<div class="ltkwidget-widget" data-rows="1" data-cols="20" data-show-frame="false" data-user-id="34832" data-padding="0">
		    <script type="text/javascript">
		      !function(d,s,id){
		          var e, p = /^http:/.test(d.location) ? 'http' : 'https';
		          if(!d.getElementById(id)) {
		              e     = d.createElement(s);
		              e.id  = id;
		              e.src = p + '://' + 'widgets.rewardstyle.com' + '/js/ltkwidget.js';
		              d.body.appendChild(e);
		          }
		          if(typeof(window.__ltkwidget) === 'object') {
		              if(document.readyState === 'complete') {
		                  __ltkwidget.init();
		              }
		          }
		      }(document, 'script', 'ltkwidget-script');
		    </script>
		    <div class="rs-adblock">
		      <img src="//assets.rewardstyle.com/images/search/350.gif" onerror="this.parentNode.innerHTML='Disable your ad blocking software to view this content.'" />
		      <noscript>JavaScript is currently disabled in this browser.  Reactivate it to view this content.</noscript>
		    </div>
			</div>
			<!-- End LTK Widget -->
		</div>
	</div>		

</section>
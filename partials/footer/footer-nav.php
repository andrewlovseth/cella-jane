<nav class="footer-nav">
	<div class="col main">
		<div class="footer-logo">
			<a href="<?php echo site_url('/'); ?>">
				<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>

		<div class="nav-links">
			<?php if(have_rows('footer_main_links', 'options')): while(have_rows('footer_main_links', 'options')): the_row(); ?>

				<a href="<?php the_sub_field('link'); ?>" class="link-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
					<?php the_sub_field('label'); ?>
				</a>
			<?php endwhile; endif; ?>

			<div class="footer-search">
				<a href="#" class="search-trigger">
					<img src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" alt="Search Icon" />
				</a>			
			</div>	

		</div>
	</div>

	<div class="col browse">
		<div class="headline footer-headline">
			<h3>Browse</h3>
		</div>

		<div class="nav-links">
			<?php if(have_rows('desktop_nav_links', 'options')): while(have_rows('desktop_nav_links',  'options')) : the_row(); ?>

			    <?php if( get_row_layout() == 'section' ): $main_cat = get_sub_field('main_category'); ?>
					<a href="<?php echo get_term_link( $main_cat ); ?>"><?php echo $main_cat->name; ?></a>
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>	
		</div>
	</div>

	<div class="col shop">
		<div class="headline footer-headline">
			<h3>Shop</h3>
		</div>

		<div class="nav-links">
			<?php if(have_rows('footer_shop_links', 'options')): while(have_rows('footer_shop_links', 'options')): the_row(); ?>

				<a href="<?php the_sub_field('link'); ?>">
					<?php the_sub_field('label'); ?>
				</a>
			<?php endwhile; endif; ?>	
		</div>
	</div>

	<div class="col follow">
		<div class="headline footer-headline">
			<h3>Follow</h3>
		</div>

		<div class="nav-links">
			<?php if(have_rows('social_nav_links', 'options')): while(have_rows('social_nav_links', 'options')): the_row(); ?>

				<a href="<?php the_sub_field('link'); ?>" rel="external">
					<?php the_sub_field('label'); ?>
				</a>
			<?php endwhile; endif; ?>	
		</div>
	</div>
	
</nav>
<section class="subscribe">

	<div class="footer-headline">
		<h3>Subscribe by Email</h3>
	</div>

	<div class="subscribe-form">
		<form>
			<div class="field name">
				<input type="text" placeholder="Name" />
			</div>

			<div class="field email">
				<input type="email" placeholder="Email Address" />
			</div>

			<div class="field submit">
				<input type="submit" value="Enter" />
			</div>
		</form>
	</div>
	
</section>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="top">

<?php wp_body_open(); ?>


	<?php get_template_part('partials/header/utility-nav'); ?>

	<?php get_template_part('partials/header/main-nav'); ?>

	<section class="main-content">

	</section> <!-- .main-content -->
	

	<?php get_template_part('partials/footer/instagram'); ?>

	<footer>
		<div class="wrapper">

			<div class="left">
				<?php get_template_part('partials/footer/footer-nav'); ?>

				<?php get_template_part('partials/footer/subscribe'); ?>				
			</div>

			<div class="right">
				<?php get_template_part('partials/footer/credits'); ?>				
			</div>

		</div>
	</footer>

	<?php get_template_part('partials/global/subscribe-modal'); ?>

	<?php get_template_part('partials/global/search-modal'); ?>

	<?php get_template_part('partials/global/policies-modal'); ?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>
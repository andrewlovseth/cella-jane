<?php

/*

	Template Name: Home

*/

get_header(); ?>
	
	<?php get_template_part('partials/home/01-featured-post'); ?>

	<?php get_template_part('partials/home/02-recent-posts-top'); ?>

	<?php get_template_part('partials/home/03-instagram'); ?>

	<?php get_template_part('partials/home/04-current-look'); ?>

	<?php get_template_part('partials/home/05-recent-posts-middle'); ?>

	<?php get_template_part('partials/home/06-favorites'); ?>

	<?php get_template_part('partials/home/07-top-five'); ?>

	<?php get_template_part('partials/home/08-health-guide'); ?>

	<?php get_template_part('partials/the-archive'); ?>

<?php get_footer(); ?>
$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('.nav-trigger').click(function(){
		$('body').toggleClass('nav-overlay-open');
		return false;
	});

	// Smooth Scroll
	$('.smooth').smoothScroll();


	// Search Trigger
	$('.search-trigger').click(function(){
		$('body').toggleClass('search-open');
		return false;
	});

	// Search Close
	$('.search-close').click(function(){
		$('body').toggleClass('search-open');
		return false;
	});


	// Subscribe Trigger
	$('.subscribe-trigger, .link-subscribe').click(function(){
		$('body').toggleClass('subscribe-open');
		return false;
	});

	// Search Close
	$('.subscribe-close').click(function(){
		$('body').toggleClass('subscribe-open');
		return false;
	});


	// Policies Trigger
	$('.policies-trigger, .link-policies').click(function(){
		$('body').toggleClass('policies-open');
		return false;
	});

	// Policies Close
	$('.policies-close').click(function(){
		$('body').toggleClass('policies-open');
		return false;
	});


	// Shop Subnav Hover
	$('.utility-nav .link-shop, .shop-trigger').hover(
		function(){
			$(this).addClass('hover');
			$('body').addClass('shop-open');
		},
		function(){
			$(this).removeClass('hover');
			$('body').removeClass('shop-open');
		}
	);

	$('.utility-nav .link-shop, .shop-trigger').click(function(){
		return false;
	});


	// Desktop Nav Hover
	$('.desktop-nav .main-link').hover(
		function(){ $(this).addClass('hover').removeClass('normal') },
		function(){ $(this).removeClass('hover').addClass('normal') }
	);


});

// Escape -- Kill All Modals/Navs
$(document).keyup(function(e) {
	
	if (e.keyCode == 27) {
		$('body').removeClass('search-open nav-overlay-open subscribe-open policies-open');
	}

});
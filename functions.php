<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/


// Remove Admin bar from front-end
show_admin_bar( false );


// Theme Support for title tags, post thumbnails, HTML5 elements, feed links
add_theme_support( 'title-tag' );

add_theme_support( 'post-thumbnails' );

add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));

add_theme_support( 'automatic-feed-links' );

if ( ! function_exists( 'wp_body_open' ) ) {
    function wp_body_open() {
        do_action( 'wp_body_open' );
    }
}





/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/



/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/




// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {
    // Add Adobe Fonts and style.css
    wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/mip4pbn.css' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );

    // Register and noConflict jQuery 3.2.1
    wp_register_script( 'jquery.3.2.1', 'https://code.jquery.com/jquery-3.2.1.min.js' );
    wp_add_inline_script( 'jquery.3.2.1', 'var jQuery3_2_1 = $.noConflict(true);' );

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.2.1' ) );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.2.1' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts', 0 );




// Add this to the functions.php file of your WordPress theme
// It filters the mime types using the upload_mimes filter hook
// Add as many keys/values to the $mimes Array as needed

function my_custom_upload_mimes($mimes = array()) {

    // Add a key and value for the CSV file type
    $mimes['svg'] = "image/svg+xml";

    return $mimes;
}

add_action('upload_mimes', 'my_custom_upload_mimes');







/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}



if( function_exists('acf_add_options_page') ) {
    
    // add parent
    $parent = acf_add_options_page(array(
        'page_title'    => 'Site Options',
        'menu_title'    => 'Site Options',
        'redirect'      => false
    ));
    
    
    // add sub page
    acf_add_options_sub_page(array(
        'page_title'    => 'Homepage',
        'menu_title'    => 'Homepage',
        'parent_slug'   => $parent['menu_slug'],
    ));
}

// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');